# DEPRECATED: Moved to https://notify.me/faq

# What is Notify? 

Notify is a platform that allows you to subscribe directly to content creators, their notifications open content on the **native** platform via deep link.
Users get a clean and simple UI to manage their subscriptions and control what notifications they receive. Creators can manually send their notifications, or rely on our automatic service. We also have a browser extention for creators to help detect livestreams and video uploads at the precice time of publish. Notify supports all major social media platforms. The app features a chronological feed with category filters, loaded with a variety of creator/community building features such as timelines, streaks, tips (monetization), direct messaging, directory for official shows and podcasts, discover & trending pages and much more.

<a href="https://notify-me.is-a-good.site/TtVb6Dyl.png" target="_blank"><img src="https://notify-me.is-a-good.site/TtVb6Dyl.png" height="190px" alt="notify_homepage"/></a>
<a href="https://notify-me.is-a-good.site/K4nJuOVB.png" target="_blank"><img src="https://notify-me.is-a-good.site/K4nJuOVB.png" height="190px" alt="notify_homepage"/></a>
<a href="https://notify-me.is-a-good.site/33uubleM.png" target="_blank"><img src="https://notify-me.is-a-good.site/33uubleM.png" height="190px" alt="notify_homepage"/></a>
<a href="https://notify-me.is-a-good.site/dJRmK1ch.png" target="_blank"><img src="https://notify-me.is-a-good.site/dJRmK1ch.png" height="190px" alt="notify_homepage"/></a>
<a href="https://notify-me.is-a-good.site/77Gzh2jC.png" target="_blank"><img src="https://notify-me.is-a-good.site/77Gzh2jC.png" height="190px" alt="notify_homepage"/></a>
<a href="https://notify-me.is-a-good.site/SMHlRDZK.png" target="_blank"><img src="https://notify-me.is-a-good.site/SMHlRDZK.png" height="190px" alt="notify_homepage"/></a>
<a href="https://notify-me.is-a-good.site/M1GmB1Ip.jpg" target="_blank"><img src="https://notify-me.is-a-good.site/M1GmB1Ip.jpg" height="190px" alt="notify_homepage"/></a>

# Why Notify? 
Today there are many different platforms for videos, music, articles and streams- managing subscriptions and notification settings across all of these can be a pain, especially when we really only care about the creators. It's also a widely known issue on platforms like YouTube to have issues with sub-boxes and notifications not being delivered. Our solution not only provides security to creators that rely financially on being able to reach their subscribers, but to the users that just want a simple list of things they're subscribed to and the control to manage it all at a glance.

# FAQ:
### Do you do this full-time?
Yes, only thanks to the support on Twitch!
### What did you do before this?
I've been a YouTuber since age 15, my channel gained traction after I posted vlogs about my personal life, from there I did many things to earn money via that audience, merchandising, sponsorship deals, acting work- etc. Though I lost interest in pursuing that in the last two years and have been looking for the next road, which I think I've found!
### What's the full tech stack?
React Native for the mobile app, VueJS for the webapp, React for the browser extention, Electron for desktop app, [PulseJS](https://github.com/jamiepine/pulse) for front-end application logic (Mobile, Web, Extention & Desktop), NodeJS/Express for our Rest API, Elixir for the realtime/notification server, NodeJS for the PubSub server, PostgreSQL for the Database, Firebase Cloud Messaging for webpush notifications, and GCP services for deployment. Our full stack is on Stackshare: [https://stackshare.io/notify](https://stackshare.io/notify)
### Who designs the UI?
At the moment, all the design, branding and UI has been me, we've had many people in Discord attempting to come up with a logo for the first 6 months, after hundreds of designs a Twitch chat member called **DurGaV** came up with the current concept for the N logo, I then created the vector with thicker, more rounded dimensions that we currently use today!
### How will Notify earn revenue?
This is a common question, and we do have a plan! (albeit not set in stone)
For now, and for launch day, we will rely on crowdfunding and merchandise to cover server costs. After that we will be looking for venture capital, once we have an active user base. 
In the long run, advertising will be an option, but nothing intrusive and we will not *ever* restrict control over your feed to prioritize advertisements. Another option is our planned tips feature, think of it similar to Twitch bits, a quick way to support the creators you follow, we'd take a small fee at the time of purchase of the "tips". Paid subscriptions are a possibility for channels that want to restrict the delivery of content behind a paywall, or offer extra content, this is completely up to the channel owner. Lastly we're considering adding some extra features that might be behind a subscription, with a similar concept to Discord Nitro, very small cosmetic or handy features to add to the experience. 
### Do you rely on platform APIs?
No! Our system can function completely independent of APIs, we use OpenGraph to grab titles and thumbnails, the same as embedding links on Twitter. Creators can share content one of three ways, all of which do not make any API calls- 1) manually, via pasting a link, 2) automatically via our browser extension, upload / post pages on enabled platforms trigger automatic sharing, we've got verification and duplication protection covered! 3) automatically via WebSub, an RSS-style subscription which could be considered part of platforms APIs- a switch on each platform connection called "automatic" tells our WebSub server to listen for new signals.
### Are there any other platforms like Notify?
Nothing major! In the eight months of streaming no one has found anything that tackles this problem like we do. Most other concepts around "Notify" seem more closely related to Google Alerts than what we've conceptualised, our platform relies on the creator to supply and curate their content, unlike others, like alerts, listen to links, feeds or topics. We're marketing towards content creators and streamers first, which is where we've focused our product design. Our system is built to be adoptable by a younger audience, as well as catering to the typical YouTube viewer. 
### Is it easy to get started with Notify?
From the get-go, I knew it would have to be dead simple to start using, because ultimately content creators have to tell their audiences to get Notify to follow them. When a user follows a Notify link *notify.me/jamie*, they'll be directed to the relevant app store, all they need to do is hit download, and the app will set itself up, automatically subscribing to the creator that referred them. If all they want to do is get notified about that one creator, they need not create an account!
### How do you plan to get users?
On a zero budget this might seem like a daunting task, but I plan to leverage my reach on YouTube and hope that word spreads within the YouTube community, before I started the project I spoke to many friends that are fulltime YouTubers, some with very decent followings and they seemed pretty hyped about the concept. It was because of them that I was encouraged to pursue this project, so if all goes well and they like the finished product they'll just need to tell their audience to follow them on Notify via a link in there description- a similar strategy to patreon. We also have a channel reservation program where some of the biggest names have automated channels that can be claimed if/when they choose to signup, in the time being our users can still recieve their notifications through our system.
### What if I hate notifications?
Using Notify, you're likely to see a decrease in the notifications you receive as you're in complete control over your subscriptions and we give you the tools to help manage and quality control your subscriptions. You can also use the many great features of notify without notifications on.
### How do I get involved?
The best way is to join our Discord and interact, test and find bugs at any chance you get, who knows- if you're keen enough you could find your way onto the team, more than 18 chatters supporters have already contributed, 6 of which are core members of the Notify team!
### What platforms is Notify on?
We currently have a webapp (available now on notify.me), an iOS and android app (in private testing currently) and a planned browser extension & desktop app for all major platforms and browsers
### Have you worked on any other projects? 
I made WordPress themes when I was 16, I made a WordPress forum in 2014 for my followers on YouTube, other than that- nothing! This is my first "app" and certainly my first tech company.